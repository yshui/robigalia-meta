= Robigalia's Official Logo
Corey Richardson
v1 2017-01-04: Indicate inspiration

Robigalia has been without a logo for over a year. It's been in the back of my
mind that any self-respecting open source project needs a logo. I took some
design classes in high school and don't have a budget, so I made one myself!

Here's the end result:

image::https://robigalia.org/logo.svg[Robigalia logo, width=740, height=740] 

I had some requirements when designing the logo. First, it should be simple
enough that it looks good as a favicon (smaller than 64x64). It should also look
good in monochrome or in restricted color palettes. The simpler, the better.

When I was brainstorming ideas for the logo, I got hung up trying to incorporate
the idea of wheat, crop disease, agriculture, or Roman themes, owing to the
project's name. Eventually I remembered about the
http://www.ccelian.com/concepca.html[Elian script]. I started doodling various
words in Elian. A full word was too complicated. I focused on a particular
cluster of letters that fit together well: "rbg". This is also a shorthand for
"Robigalia", which works out well!

What you see above is an SVG (created in Inkscape) inspired those three letters
in the Elian script. For symmetry, the actual letters are "rbp", but it doesn't
bother me to not directly use the Elian script. The lines and the whitespace
between them are all the same width, which helps the SVG scale down to small
resolutions well. The cyan color on the dark background reminds me of Tron. I
also think it kinda looks like a sideways 8-bit jellyfish. eternaleye said it
looks like a push button schematic.

I think it's kinda neat, and look forward to seeing some awesome desktop themes
using it once we have a windowing system!
