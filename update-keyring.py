#!/usr/bin/env python

from __future__ import print_function
import subprocess
import sys
import os
import itertools

# vague approximation of correct. solution: review changes to mailmap

mailmap = sys.argv[1]

os.environ['GNUPGHOME'] = os.path.relpath('.gnupg', os.path.dirname(mailmap))

key_lines = [
        l.split(b' # ')[-1] 
        for l in open(mailmap, 'rb')
        if b' # ' in l]
valid_keys = []
for line in key_lines:
    valid_keys.extend(map(bytes.strip, line.split(b' ')))

for key in valid_keys:
    print("Fetching key {}".format(key))
    try:
        subprocess.call(['gpg2', '--keyserver', 'pool.sks-keyservers.net', '--recv-keys', key])
    except subprocess.CalledProcessError:
        print("Failed!")
    print("Success!")
